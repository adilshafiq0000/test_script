#!/usr/bin/env python
#
# A hook script to check the commit log message.
# Called by "git commit" with one argument, the name of the file
# that has the commit message.  The hook should exit with non-zero
# status after issuing an appropriate message if it wants to stop the
# commit.  The hook is allowed to edit the commit message file.
#
import sys

# Read passed message through argument.
messageFile = sys.argv[1]
file = open(messageFile, 'r')
lines = file.readlines()

foundcolon = False
# colonok = True
tabok = True
tabwocolonok = True
linesizeok = True
nofailok = True
mergefound = False
commit_num = 0
# print("Length")
# print(len(lines))

for i in range(len(lines)):
        
    if(i+1 < len(lines)):
        if(lines[i+1].find("Merge branch") != -1):
            mergefound = True
        else:
            mergefound = False
    if(mergefound == False):
        colon_index = lines[i].find(":")
        # print(lines[i])
        # print(i)
        # print(colon_index)
        if(colon_index != -1):
            foundcolon = True
            if(tabok):
                if(lines[i][colon_index+1] == "\t"):
                    # print("Yes! TAB")
                    tabok = True
                else:
                    tabok = False
        elif(foundcolon):
            if(tabwocolonok):
                if(lines[i][0] != "\t"):
                    tabwocolonok = False


        if(foundcolon):
            # print("Size : ")
            # print(len(lines[i]))
            if(linesizeok):
                if(len(lines[i]) > 104):
                    linesizeok = False

if(mergefound == False):
    if(not(tabok)):
        print("Tab not found after colon(:)")
    if(not(foundcolon)):
        print("No Colon Found in Commit")
    if(not(linesizeok)):
        print("Total Characters in each line should be 104 at max (not applied for headings)")
    if(not(tabwocolonok)):
        print("Each new line should have TAB in File Description")
    if(foundcolon and tabok and linesizeok and tabwocolonok):
        print("Commit-Passed")
    else:
        print("Commit-Denied")
        nofailok = False
else:
    print("Merge Found, commit ignored!")

if(nofailok):
    print("All Commits Passed!")
    exit(0)
else:
    print("One of the Commits Failed!")
    exit(-1)
