from __future__ import annotations
from setuptools import setup
from setuptools import find_packages

setup(

    name='pre-commit-hooks',
    description='Some out-of-the-box hooks for pre-commit',
    url='https://gitlab.com/adilshafiq0000/test_script',
    version='2.0.0',

    author='Aqib',
    author_email='aqib@gmail.com',

    platforms='windows',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],

    packages=find_packages('.'),
    install_requires=[
        'fuzzywuzzy',
        'python-Levenshtein',
        'sys'
    ],

    entry_points={
        'console_scripts': [
            'commit-format = hooks.commit-format:main',
        ],
    },
)
